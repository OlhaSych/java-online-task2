package com.sych;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        System.out.println("Please enter number of quests :");
        Scanner scanner = new Scanner(System.in);
        int numberOfGuests = scanner.nextInt();
        System.out.println("Please enter number of tests :");
        int numberOfTests = scanner.nextInt();
        scanner.close();
        if (numberOfGuests <= 1 || numberOfTests <= 0) {
            System.out.println("Number of guests must be > 1 and number of tests must be>0!");
        } else {
            RumorsSpread rumors = new RumorsSpread();
            rumors.rumor(numberOfGuests, numberOfTests);

        }
    }


}
