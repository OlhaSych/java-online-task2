package com.sych;

import java.util.Random;

/**
 * This class is used to describe the basic logic of the project
 *
 * @author Sych Olha
 */
public class RumorsSpread {
    /**
     * Represents arrays of guest of the party
     */
    private Guests[] guest;
    /**
     * Represents number of guests who heard the rumor
     */
    private int howManyGuestsHeard;
    /**
     * Represents count of all guest og party who heard the rumor
     */
    private int allHeard;
    /**
     * Represents total amount of all guest who heard the rumor of all attempts
     */
    private int totalNumberOfGuestsWhoHeard;

    /**
     * This method is used to describe how the rumor spread between guests
     *
     * @param numberOfGuests This is a number of all guests of party
     * @param numberOfTests  This ia a number of all attempts
     */
    public void rumor(int numberOfGuests, int numberOfTests) {
        Random random = new Random();

        guest = new Guests[numberOfGuests];
        int guestWhoLastKnown;
        for (int i = 0; i < numberOfTests; i++) {
            int guestWhoJustKnown = 0;
            howManyGuestsHeard = 0;
            for (int j = 0; j < numberOfGuests; j++) {
                guest[j] = new Guests(false);
            }
            guest[0].setHeard(true);
            int guestWhoAlreadyKnow = random.nextInt(numberOfGuests);
            while (guestWhoJustKnown == guestWhoAlreadyKnow) {
                guestWhoAlreadyKnow = random.nextInt(numberOfGuests);
            }
            while (!guest[guestWhoAlreadyKnow].isHeard()) {
                guest[guestWhoAlreadyKnow].setHeard(true);
                guestWhoLastKnown = guestWhoJustKnown;
                guestWhoJustKnown = guestWhoAlreadyKnow;
                guestWhoAlreadyKnow = random.nextInt(numberOfGuests);
                while (guestWhoJustKnown == guestWhoAlreadyKnow || guestWhoLastKnown == guestWhoAlreadyKnow) {
                    guestWhoAlreadyKnow = random.nextInt(numberOfGuests);
                }
            }
            numberWhoHeard(numberOfGuests);
        }
        printResults(numberOfTests);
    }

    /**
     * This method is used to count how many guests heard the rumors
     *
     * @param numberOfGuests This is a number of all guests of party
     */
    private void numberWhoHeard(int numberOfGuests) {
        for (int j = 0; j < numberOfGuests; ++j) {
            if (guest[j].isHeard() == true) {
                howManyGuestsHeard++;
            }
        }
        if (howManyGuestsHeard == numberOfGuests) {
            allHeard++;
        }
        totalNumberOfGuestsWhoHeard = totalNumberOfGuestsWhoHeard + howManyGuestsHeard;

    }

    /**
     * This method is used to show final results of project
     *
     * @param numberOfTests This ia a number of all attempts
     */
    private void printResults(int numberOfTests) {
        double probabilityThatAllHeard = (double) allHeard / numberOfTests * 100;
        double expectedNumberOgGuestsWhoHeard = (double) totalNumberOfGuestsWhoHeard / numberOfTests;
        System.out.println("The probability that everyone at the party (except Alice) " +
                "will hear the rumor before it stops propagating is :" + probabilityThatAllHeard + " %");
        System.out.println("An estimate of the expected number " +
                "of people to hear the rumor is :" + expectedNumberOgGuestsWhoHeard);

    }
}
