package com.sych;

/**
 * This class is used to create guests of party
 */
public class Guests {
    /**
     * Shows whether the rumor has been heard
     */
    private boolean heard;

    /**
     * Class constructor
     * @param heard This parameter shows whether the rumor has been heard
     */
    public Guests(boolean heard) {
        this.heard = heard;
    }

    /**
     * This method is used to get value of the field heard
     * @return the value of the field heard
     */
    public boolean isHeard() {
        return heard;
    }

    /**
     * This method is used to set value of the field heard
     * @param heard This parameter shows whether the rumor has been heard
     */
    public void setHeard(boolean heard) {
        this.heard = heard;
    }

    @Override
    public String toString() {
        return "Guests{" +
                "heard=" + heard +
                '}';
    }
}
